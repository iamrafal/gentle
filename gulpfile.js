'use strict';
var buildDir = './build';
var config = {
    assetsPath: '..'
};

var assets_version = Date.now();

var gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    plumber = require('gulp-plumber'),
    rename = require('gulp-rename'),
    clip = require('gulp-clip-empty-files'),
    autoprefixer = require('gulp-autoprefixer'),
    browserSync = require('browser-sync'),
    nunjucksRender = require('gulp-nunjucks-render'),
    data = require('gulp-data'),
    sequence = require('run-sequence'),
    del = require('del'),
    tinypng = require('gulp-tinypng'),
    livereload = require('gulp-livereload');

// Scripts
gulp.task('scripts', function() {
    return gulp.src('./src/assets/js/**/*.js')
        .pipe(plumber())
        // .pipe(uglify())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest(buildDir + '/assets/js'))
        .pipe(livereload());
});

// Styles
gulp.task('styles', function() {
    return gulp.src(['src/assets/sass/**/*.+(scss|sass)'])
        .pipe(clip())
        .pipe(plumber())
        .pipe(sass({
            style: 'compressed'
        }))
        .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
        .pipe(gulp.dest(buildDir + '/assets/css/'))
        .pipe(livereload());
});

// Copy files
gulp.task('copy', function() {
    return gulp.src([
        'src/assets/vendor/**/*.*',
        'src/assets/img/**/*.*',
        'src/assets/fonts/**/*.*'
    ], {base: 'src'
    })
    .pipe(gulp.dest(buildDir));

});

// Delete build
gulp.task('clean:build', function () {
    return del(buildDir);
});

// Browser sync
gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: buildDir,
            directory: true
        }
    });
});

// Compile nunjucks files
gulp.task('nunjucks', function() {
    return gulp.src('src/templates/pages/**/*.+(html|njk)')
        .pipe(plumber())
        .pipe(data(function () {
            return {
                config: config,
                assets_version: assets_version
            }
        }))
        .pipe(nunjucksRender({
            path: ['src/templates']
        }))
        // .pipe(rename({ extname: '.php' }))
        .pipe(gulp.dest(buildDir))
        .pipe(livereload());
});

gulp.task('serve', function(done) {
    var express = require('express');
    var app = express();
    var server = require('http').createServer(app);
    app.use(express.static(__dirname + '/build'));
    server.listen(4000, '0.0.0.0', function () {
        done();
    });
});

// Build project
gulp.task('build', function () {
    sequence(
        ['clean:build'],
        ['copy', 'scripts', 'styles'],
        'nunjucks'
    );
});

// Watch file changes
gulp.task('watch', function () {
    gulp.watch("./src/assets/js/**/*.js", ['scripts']);
    gulp.watch("./src/assets/sass/**/*.sass", ['styles']);
    gulp.watch("./src/assets/sass/**/*.scss", ['styles']);
    gulp.watch("./src/templates/**/*.+(html|njk)", ['nunjucks']);
    livereload.listen();
});


// Default task
gulp.task('default', function () {
    sequence('build', 'watch', 'serve');
});