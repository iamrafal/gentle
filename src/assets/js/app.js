'use strict';

$(function () {
    $('.mobileMenu').on('click', function (e) {
        e.stopPropagation();
    });

    var $toggleMobileMenuBtn = $('.js-menuTrigger'),
        $pageContainer = $('#page-container');

    $toggleMobileMenuBtn.on('click', function (e) {
        e.stopPropagation();
        var $this = $(this);
        if ($this.hasClass('-animate')) {
            $this.removeClass('-animate');
            $pageContainer.removeClass('menu-open');
        } else {
            $this.addClass('-animate');
            $pageContainer.addClass('menu-open');
        }
    });

    $('.pushContent').on('click', function () {
        $pageContainer.removeClass('menu-open');
        $toggleMobileMenuBtn.removeClass('-animate');
        $(".mobileMenu-level").removeClass('-visible');
    });

    $('.mobileMenu .js-levelTrigger').on('click', function() {
       var $this = $(this),
           $currectLevel = $this.next(".mobileMenu-level");

        if ($currectLevel.hasClass('-visible')) {
            $currectLevel.removeClass('-visible');
        } else {
            $currectLevel.addClass('-visible');
        }
    });

    $('.mobileMenu .mml-back').on('click', function() {
        $(this).parent(".mobileMenu-level").removeClass('-visible');
    });

    $('.productCard .heart-button').on('click', function() {
        $(this).toggleClass('-liked');
    });

    var $dropdownBox = $('.dropdownComponent');

    $(document).click(function () {
        if($dropdownBox.is(':visible')) {
            $dropdownBox.removeClass('-visible');
        }
    });

    $(document).on('click', '.dropdownComponent, .dropdownComponent-menu', function(e) {
        e.stopPropagation();
    });

    $('.js-dropdown-trigger').click(function () {

        var $this,
            $dropdownBoxCurrent;

        $this = $(this);
        $dropdownBoxCurrent = $this.closest('.dropdownComponent');

        if($dropdownBoxCurrent.hasClass('-visible')) {
            $dropdownBoxCurrent.removeClass('-visible');
            return;
        }

        $('.dropdownComponent').removeClass('-visible');
        $dropdownBoxCurrent.addClass('-visible');
    });
});
