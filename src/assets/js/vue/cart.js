new Vue({
    el: '.vueGentle',
    data: {
        total: 0,
        itemsInCartNumber: 0,
        items: [
            {
                id: 1,
                imageSrc: 'assets/img/products/example1.jpg',
                name: 'Tailored Fit iris Blue Twill Jacket',
                brand: 'Vistula',
                likes: '22',
                price: 282.99,
            },
            {
                id: 2,
                imageSrc: 'assets/img/products/example1.jpg',
                name: 'Marynarka Batalion - 1A BG1-02-16A',
                brand: 'Vistula',
                likes: '5',
                price: 329.90,
            },
            {
                id: 3,
                imageSrc: 'assets/img/products/example1.jpg',
                name: 'Marynarka Alto BE1-02-34A',
                brand: 'Bytom',
                likes: '15',
                price: 215.19,
            },
            {
                id: 4,
                imageSrc: 'assets/img/products/example1.jpg',
                name: 'Marynarka Afrygola BH1-02-60B',
                brand: 'Bytom',
                likes: '2',
                price: 154.49,
            }
        ],
        cart: []
    },
    methods: {
        increaseQty: function(index) {
            this.cart[index].qty++;
            this.total += this.cart[index].price;
            this.itemsInCartNumber++;
        },
        decreaseQty: function(index) {
            this.cart[index].qty--;
            this.total -= this.cart[index].price;
            this.itemsInCartNumber--;
            if (this.cart[index].qty === 0) {
                this.cart.splice(index, 1);
            }
        },
        clearCart: function() {
            this.cart.splice(0, this.cart.length);
            this.total = 0;
            this.itemsInCartNumber = 0;
        },
        addItem: function(index) {
            this.total += parseFloat(this.items[index].price);
            var item = this.items[index];
            var found = false;
            this.itemsInCartNumber++;
            for (var i = 0; i < this.cart.length; i++) {
                if (this.cart[i].id === item.id) {
                    found = true;
                    this.cart[i].qty++;
                } else {

                }
            }
            if (!found) {
                this.cart.push({
                    id: item.id,
                    imageSrc: item.imageSrc,
                    name: item.name,
                    price: item.price,
                    qty: 1
                });
            }
        },
        removeItem: function(index) {
            var totalQtyPrice = this.cart[index].qty * this.cart[index].price;
            this.total -= totalQtyPrice;
            this.itemsInCartNumber -= this.cart[index].qty;
            this.cart.splice(index, 1);
        }
    },
    mounted() {
        if (localStorage.getItem('cart')) {
            this.cart = JSON.parse(localStorage.getItem('cart'));
        }
        if (localStorage.getItem('itemsInCartNumber')) {
            this.itemsInCartNumber = JSON.parse(localStorage.getItem('itemsInCartNumber'));
        }
        if (localStorage.getItem('total')) {
            this.total = JSON.parse(localStorage.getItem('total'));
        }
    },
    watch: {
        cart: {
            handler() {
                localStorage.setItem('cart', JSON.stringify(this.cart));
            },
            deep: true,
        },
        itemsInCartNumber: {
            handler() {
                localStorage.setItem('itemsInCartNumber', JSON.stringify(this.itemsInCartNumber));
            },
            deep: true,
        },
        total: {
            handler() {
                localStorage.setItem('total', JSON.stringify(this.total));
            },
            deep: true,
        }
    },
    filters: {
        currencydecimal (value) {
            return value.toFixed(2)
        }
    },
});