var products =
    [
        {
            id: 1,
            title: "Tailored Fit iris Blue Twill Jacket",
            brand: "Vistula",
            category: "Koszule",
            numberOfLikes: 7,
            price: 282.99,
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam at risus nec orci congue posuere. In finibus vestibulum est, vitae porta purus pharetra id. Suspendisse congue in ligula vitae luctus. Nulla faucibus lectus sit amet lectus luctus convallis.",
            ean: "6421951096002",
            imageSrc: "http://i1084.photobucket.com/albums/j407/861025/0011/yujt5kyujh.jpg"
        },
        {
            id: 2,
            title: "Tailored Fit iris Blue Twill Jacket",
            brand: "Gentle",
            category: "Koszule",
            numberOfLikes: 2,
            price: 12.84,
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam at risus nec orci congue posuere. In finibus vestibulum est, vitae porta purus pharetra id. Suspendisse congue in ligula vitae luctus. Nulla faucibus lectus sit amet lectus luctus convallis.",
            ean: "6421951096002",
            imageSrc: "http://i1084.photobucket.com/albums/j407/861025/0011/yujt5kyujh.jpg"
        },
        {
            id: 3,
            title: "Tailored Fit iris Blue Twill Jacket",
            brand: "Private Label",
            category: "Spodnie",
            numberOfLikes: 53,
            price: 102.84,
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam at risus nec orci congue posuere. In finibus vestibulum est, vitae porta purus pharetra id. Suspendisse congue in ligula vitae luctus. Nulla faucibus lectus sit amet lectus luctus convallis.",
            ean: "6421951096002",
            imageSrc: "http://i1084.photobucket.com/albums/j407/861025/0011/yujt5kyujh.jpg"
        }
    ];

function createProductHTML(product)
{
    return "<article class=\"productCard\" itemscope=\"\" itemtype=\"http://schema.org/Product\">\n" +
        "    <a href=\"#!\">\n" +
        "    <img src=" + product.imageSrc + " alt=\"\">\n" +
        "    </a>\n" +
        "    <div class=\"productCard-details\">\n" +
        "    <h2 class=\"productTitle\" itemprop=\"name\">" + product.title + "</h2>\n" +
        "<span class=\"productBrand\">" + product.brand + "</span>\n" +
        "    <button class=\"heart-button\" type=\"button\">\n" +
        "    <span class=\"heart-container\">\n" +
        "    <span class=\"icon heart\"></span>\n" +
        "    </span>\n" +
        "    <span class=\"number\">" + product.numberOfLikes + "</span>\n" +
        "    </button>\n" +
        "    </div>\n" +
        "    <div class=\"buttons-group\" itemscope=\"\" itemtype=\"http://schema.org/Offer\">\n" +
        "    <button data-id=" + product.id + " class=\"button -small -highlight js-add-cart-product\" type=\"button\">\n" +
        "    <span class=\"fas fa-cart-plus\"></span><span itemprop=\"price\"> " + product.price + "</span><span> PLN</span>\n" +
        "</button>\n" +
        "<button class=\"button -small -empty\" type=\"button\">\n" +
        "    Szczegóły\n" +
        "    </button>\n" +
        "    </div>\n" +
        "    </article>";
}

function filterAndPrintProducts()
{
    var categories = [],
    brands = [],
    temp = products,
    priceFrom = 0,
    priceTo = 0;

    priceFrom = parseFloat($("#js-priceSliderInputLower").val().split(' ')[0].replace(".",","));
    priceTo = parseFloat($("#js-priceSliderInputUpper").val().split(' ')[0].replace(".", ","));

    categories = $('input[name=categories]:checked').map(function () {
        return this.value;
    }).get();

    brands = $('input[name=brands]:checked').map(function () {
        return this.value;
    }).get();

    if(categories.length > 0)
    {
        temp = $.grep(temp, function( n, i ) {
            for(var i = 0; i < categories.length; i++)
            {
                return n.category===categories[i];
            }
        });
    }
    if(brands.length > 0)
    {
        temp = $.grep(temp, function( n, i ) {
            for(var i = 0; i < brands.length; i++)
            {
                return n.brand===brands[i];
            }
        });
    }
    if(priceFrom > 0)
    {
        temp = $.grep(temp, function( n, i ) {
            return n.price > priceFrom;
        });
    }
    if(priceTo > 0)
    {
        temp = $.grep(temp, function( n, i ) {
            return n.price <= priceTo;
        });
    }

    $(".productCards-list").html("");

    if(temp.length == 0)
    {
        $(".productCards-list").eq(0).html("<span>Brak produktów</span>");
    }
    else
    {
        for(var i = 0; i < Math.min(9, temp.length); i++)
        {
            $(".productCards-list").eq(0).append(createProductHTML(temp[i]));
        }

        if(temp.length > 9)
        {
            for(var i = 9; i < temp.length; i++)
            {
                $(".productCards-list").eq(1).append(createProductHTML(temp[i]));
            }
        }
    }
}

// do wykorzystania na kazdej stronie - dodawanie do, usuwanie z i aktualizowanie koszyka

// function calculatePrice()
// {
//     var price = 0;
//
//     $(".cartDropdown-price").each(function () {
//         price += parseFloat($(this).text());
//     });
//
//     $(".cartDropdown-summary > .totalprice").text(price.toFixed(2));
// }

// $(".cartDropdown-items").on("click", ".js-remove-cart-product", function()
// {
//     $(this).parent().remove();
//     refreshBasketItemsCount();
//     calculatePrice();
// });

// $(".productCards-list").on("click", ".js-add-cart-product", function()
// {
//     var id = parseInt($(this).data("id")),
//     product = $.grep(products, function( n, i ) {
//         return n.id === id;
//     })[0];
//
    $(".cartDropdown-items").append("<li class=\"cartDropdown-item\">\n" +
        "<button class=\"remove js-remove-cart-product\" type=\"button\">\n" +
        "<span class=\"fa fa-times\"></span>\n" +
        "</button>\n" +
        "<a href=\"#!\" class=\"cartDropdown-img\">\n" +
        "<img src=" + product.imageSrc + " alt=\"Example\">\n" +
        "</a>\n" +
        "<div class=\"cartDropdown-details\">\n" +
        "<a href=\"#!\" class=\"name\">\n" +
        product.title +
        "</a>\n" +
        "<div class=\"cartDropdown-pricebar\">\n" +
        "<span class=\"cartDropdown-singleprice\">" + product.price + "<small>zł / szt.</small></span>\n" +
        "<div class=\"cartDropdown-qty\">\n" +
        "<button class=\"btn js-add-qty\">\n" +
        "<span class=\"fa fa-minus\"></span>\n" +
        "</button>\n" +
        "<span class=\"qty\">1</span>\n" +
        "<button class=\"btn js-sub-qty\">\n" +
        "<span class=\"fa fa-plus\"></span>\n" +
        "</button>\n" +
        "</div> \n" +
        "<div class=\"cartDropdown-price\">\n" +
        "<span class=\"price\">" + product.price + "</span> zł\n" +
        "</div> \n" +
        "</div> \n" +
        "</div> \n" +
        "</li>");
//     refreshBasketItemsCount();
//     calculatePrice();
// });

// $(".cartDropdown-items").on("click", ".js-sub-qty", function()
// {
//     var qty = $(this).parent().find(".qty");
//     qty.text(parseInt(qty.text()) + 1);
//     calculateSinglePrice($(this).parents(".cartDropdown-item"));
// });
//
// $(".cartDropdown-items").on("click", ".js-add-qty", function()
// {
//     var qty = $(this).parent().find(".qty");
//     var number = parseInt(qty.text());
//
//     if(number === 1)
//         return;
//
//     qty.text(parseInt(qty.text()) - 1);
//     calculateSinglePrice($(this).parents(".cartDropdown-item"));
// });
//
// $(".js-removeFromBasket").on("click", function()
// {
//     $(".cartDropdown-items").html("");
//     $(".cartDropdown-summary > .totalprice").text("0.00 zł");
// });
//
// function calculateSinglePrice(ele)
// {
//         var price = parseInt(ele.find(".qty").text()) * parseFloat(ele.find(".cartDropdown-singleprice").text());
//
//     ele.find(".cartDropdown-price").text(price.toFixed(2));
//     calculatePrice();
// }
//
// function refreshBasketItemsCount()
// {
//   $(".js-dropdown-trigger > .badge").text($(".cartDropdown-item").length);
// }


